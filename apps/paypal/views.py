from requests.exceptions import HTTPError

# API
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

#Tools
from datetime import datetime
from apps.paypal.eduNext import EduNextAPI
import types
import json

#Serializers
from apps.paypal.serializers import PaymentPaypalSerializer

class PaypalAPIView(APIView):
    serializer_model = PaymentPaypalSerializer

    """
        A value is established for each item to 
        know if it went up or down
    """
    ITEMS = {
        "free": 1,
        "basic": 2,
        "premium": 3
    }


    """
        Validates if the item is larger or smaller 
        and assigns it its respective date
    """
    def type_change_subscription(self,old_item,new_item):
        if not self.ITEMS[new_item] == self.ITEMS[old_item]:
            return {"UPGRADE_DATE": str(datetime.now())} if self.ITEMS[new_item] > self.ITEMS[old_item] else {"DOWNGRADE_DATE": str(datetime.now())}
        return {}


    """
        validates the payment status and assigns 
        the subscription type
    """
    def get_enabled_features(self,data):
        value = False
        subscription = "free"
        if data.validated_data['payment_status'].lower() == 'completed':
            value = True
            subscription = data.validated_data['item_name']
        return {
            "SUBSCRIPTION": subscription,
            "ENABLED_FEATURES": {
                "CERTIFICATES_INSTRUCTOR_GENERATION": value,
                "INSTRUCTOR_BACKGROUND_TASKS": value,
                "ENABLE_COURSEWARE_SEARCH": value,
                "ENABLE_COURSE_DISCOVERY": value,
                "ENABLE_DASHBOARD_SEARCH": value,
                "ENABLE_EDXNOTES": value
            }
        }


    def post(self,request):
        new_payment = self.serializer_model(data=request.data)

        if new_payment.is_valid():
            edu_next = EduNextAPI()
            try:
                result = edu_next.get_client(new_payment.validated_data['payer_id'])
                if result.status_code in [404]:
                    return Response(json.loads(result.content), status=result.status_code)
                else:
                    """
                       The data of the client returned in the request of eduNextAPI 
                       is captured and updated with the new data
                    """
                    response_json = json.loads(result.content)
                    data = response_json['data']
                    old_item = data['SUBSCRIPTION'] if 'SUBSCRIPTION' in data else False
                    data.update(self.get_enabled_features(new_payment))
                    data.update({
                        "LAST_PAYMENT_DATE": str(datetime.now())
                    })

                    """
                        Valid if you have purchased an item previously 
                        if you do not add the attribute creation date
                    """
                    if old_item:
                        data.update(self.type_change_subscription(old_item,data['SUBSCRIPTION']))
                    else:
                        data.update({"CREATION_DATE": str(datetime.now())})

                    """
                        The client's data is updated in eduNextAPI
                    """
                    try:
                        result_edit = edu_next.edit_client(response_json['id'], data=data)
                        return Response(json.loads(result_edit.content), status=result_edit.status_code)
                    except HTTPError as e:
                        return Response(e.response.json())
            except HTTPError as e:
                return Response(e.response.json())

        """
            :return all errors with a status HTTP 400
        """
        return Response(new_payment.errors,status=status.HTTP_400_BAD_REQUEST)