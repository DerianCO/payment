from django.conf.urls import url
from apps.paypal.views import PaypalAPIView

app_name = "payments"

urlpatterns = (
    url(r'^paypal/$', PaypalAPIView.as_view(), name="api_paypal"),
)
