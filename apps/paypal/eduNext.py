import re
import json
import requests

BASE_URL = 'http://localhost:8010/api/'

class EduNextAPI(object):
    def __init__(self, version='v1'):
        assert isinstance(version, str), 'version is not valid'
        self.api_url = BASE_URL + version

    def get_headers(self):
        """
        Headers to use for request
        :return:
        """
        headers = {
            "Content-Type": "application/json"
        }
        return headers

    def get_client(self, client_id):
        """
        Get data of a client
        :return:
        """
        assert client_id, 'cliend_id is not valid'
        url = self.api_url+"/customerdata/{}/".format(client_id)
        return send_request(url, method='GET')

    def create_client(self,**kwargs):
        """
        :param kwargs: params to send in request
        :return:
        """
        url = self.api_url+"/customerdata/"
        data = json.dumps(kwargs)
        return send_request(url, method='POST', headers=self.get_headers, data=data)

    def edit_client(self,client_id,**kwargs):
        """
        :param client_id: id of the client to consulting
        :param kwargs:
        :return:
        """
        assert client_id, 'Client is not valid'
        url = self.api_url+"/customerdata/{}/".format(client_id)
        data = json.dumps(kwargs)
        return send_request(url,method='PUT', headers=self.get_headers(), data=data)


def send_request(url, method='GET', data=None, headers=None):
    """
    :param method: HTTP method to use
    :param data: Data to send in case of PUT ,POST, PATCH
    :param headers: HTTP headers to use request
    :return: Return a HTTP object
    """
    assert url and method
    assert method in ['GET', 'PUT', 'DELETE', 'POST', 'PATCH']
    method = getattr(requests, method.lower())
    response = method(url=url, data=data, headers=headers)
    return response