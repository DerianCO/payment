from rest_framework import serializers


class PaymentPaypalSerializer(serializers.Serializer):
    protection_eligibility = serializers.CharField(max_length=30)
    address_status = serializers.CharField(max_length=30)
    payer_id = serializers.CharField(max_length=100)
    payment_date = serializers.CharField(max_length=120)
    payment_status = serializers.CharField(max_length=30)
    notify_version = serializers.CharField(max_length=6)
    verify_sign = serializers.CharField(max_length=255)
    receiver_id = serializers.CharField(max_length=30)
    txn_type = serializers.CharField(max_length=40)
    item_name = serializers.CharField(max_length=30)
    mc_currency = serializers.CharField(max_length=4)
    payment_gross = serializers.CharField(max_length=20)
    shipping = serializers.CharField(max_length=6)
