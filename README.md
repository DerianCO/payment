#Step by step installation
this project depends on the api sent by eduNext, therefore it is necessary that said project is running.

The default url for requests is **http://localhost:8010/api/**, this can be modified in the eduNext.py file found in **apps/paypal/**
____

###Create a virtualenv 
1. requirements:
    - python3
    - virtualenv
    > virtualenv -p python3 venv
2. use environment
    > source venv/bin/activate
 
###Install project requirments
1. open project folder
   > cd project_folder_name
2. install requirements
   > pip install -r requirements.txt

### Run project
1. run server
   > python manage.py runserver
   

#Use the project API
#### Request /payments/paypal/ [**POST**]
data to send:

    {
        "protection_eligibility": string,
        "address_status": string,
        "payer_id": string,
        "payment_date": string,
        "payment_status": string,
        "notify_version": string,
        "verify_sign": string,
        "receiver_id": string,
        "txn_type": string,
        "item_name": string,
        "mc_currency": string,
        "payment_gross": string,
        "shipping": string
    }
    
###Response HTTP_200_OK
Respond to updated customer data

    {
    "id": "0dab2191-7e9b-4523-a84d-2d2b51219444",
    "data": {
        "banner_message": "<p><span>Welcome</span> to Mr X's website</p>",
        "LAST_PAYMENT_DATE": "2018-06-16 15:42:05.849774",
        "theme_name": "Tropical Island",
        "user_profile_image": "https://i.imgur.com/LMhM8nn.jpg",
        "UPGRADE_DATE": "2018-06-16 15:02:25.729226",
        "DOWNGRADE_DATE": "2018-06-16 15:02:15.114954",
        "ENABLED_FEATURES": {
            "CERTIFICATES_INSTRUCTOR_GENERATION": true,
            "ENABLE_COURSEWARE_SEARCH": true,
            "ENABLE_EDXNOTES": true,
            "ENABLE_DASHBOARD_SEARCH": true,
            "INSTRUCTOR_BACKGROUND_TASKS": true,
            "ENABLE_COURSE_DISCOVERY": true
        },
        "displayed_timezone": "America/Bogota",
        "language_code": "en",
        "CREATION_DATE": "2018-06-16 15:01:18.105354",
        "user_email": "barack@aol.com",
        "SUBSCRIPTION": "premium"
    }
}
    
    
###Response HTTP_400_BAD_REQUEST
This answer is due to missing data in the json

    {
        "field": [
            "This field is required."
        ]
    }

###Response HTTP_404_NOT_FOUND
This response is due to the fact that the client id does not exist in eduNextAPI

    {
        "detail": "Not found."
    }